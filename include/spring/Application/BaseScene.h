#pragma once
#include <spring\Framework\IScene.h>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

namespace Spring
{
	class BaseScene : public IScene
	{
		Q_OBJECT

	public:

		explicit BaseScene(const std::string& ac_szSceneName);

		void createScene() override;

		void release() override;

		~BaseScene();
	private:
		void createGUI();

		QWidget *centralWidget;
		QLabel *helloLabel;
		QLineEdit *lineEditWithName;
		QPushButton *backButton;
		QMenuBar *menuBar;
		QToolBar *mainToolBar;
		QStatusBar *statusBar;

		private slots:
		void mf_backButton();
		
	};

}
