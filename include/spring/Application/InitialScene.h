#pragma once
#include <spring\Framework\IScene.h>
#include <qgridlayout.h>
#include <qspinbox.h>
#include <qpushbutton.h>
#include <qlabel>
#include <qlineedit.h>
#include <qobject.h>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

namespace Spring
{
	class InitialScene : public IScene
	{
		Q_OBJECT

	public:

		explicit InitialScene(const std::string& ac_szSceneName);

		void createScene() override;

		void release() override;

		~InitialScene();

	private:
		void createGUI();

		QWidget *centralWidget;
		QGridLayout *gridLayout_2;
		QSpacerItem *horizontalSpacer;
		QPushButton *buttonSayHello;
		QSpacerItem *horizontalSpacer_2;
		QSpacerItem *verticalSpacer;
		QSpacerItem *verticalSpacer_2;
		QSpacerItem *verticalSpacer_3;
		QLineEdit *lineEditName;
		QMenuBar *menuBar;
		QToolBar *mainToolBar;
		QStatusBar *statusBar;

		private slots:
		void mf_sayHelloButton();

	};
}
