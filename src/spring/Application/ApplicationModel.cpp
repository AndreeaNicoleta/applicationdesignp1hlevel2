#include "..\..\..\include\spring\Application\ApplicationModel.h"
#include <spring\Application\InitialScene.h>
#include <spring\Application\BaseScene.h>

const std::string initialSceneName = "InitialScene";
const std::string baseSceneName = "BaseScene";

namespace Spring
{
	ApplicationModel::ApplicationModel()
	{
	}

	void ApplicationModel::defineScene()
	{
		IScene* initialScene = new InitialScene(initialSceneName);
		m_Scenes.emplace(initialSceneName, initialScene);

		IScene* baseScene = new BaseScene(baseSceneName);
		m_Scenes.emplace(baseSceneName, baseScene);
	}

	void ApplicationModel::defineInitialScene()
	{
		mv_szInitialScene = initialSceneName;
	}

	void ApplicationModel::defineTransientData()
	{
		//add initial values for all transient data 

		std::string nameForLineEdit = "Text your name";
		m_TransientData.emplace("NameForLineEdit", nameForLineEdit);

		std::string nameForLineEditSecondScene = "Name";
		m_TransientData.emplace("NameForLineEditSecondScene", nameForLineEditSecondScene);

	}
}
