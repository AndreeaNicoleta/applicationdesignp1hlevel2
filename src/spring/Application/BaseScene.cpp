#include "..\..\..\include\spring\Application\BaseScene.h"

namespace Spring
{
	BaseScene::BaseScene(const std::string& ac_szSceneName) : IScene(ac_szSceneName)
	{
	}

	void BaseScene::createScene()
	{
		createGUI();
		//std::string nameForLineEditBase = boost::any_cast<std::string>(m_TransientDataCollection["NameForLineEdit"]);

		//lineEditWithName->setText(QString(nameForLineEditBase.c_str()));
		//m_uMainWindow->(QString(appName.c_str()));
		//m_uMainWindow->setWindowTitle("Second window");
		QObject::connect(backButton, SIGNAL(released()), this, SLOT(mf_backButton()));
	}

	void BaseScene::release()
	{
		delete centralWidget;
	}
	
	BaseScene::~BaseScene()
	{
	}

	void BaseScene::createGUI()
	{
		m_uMainWindow->resize(400, 300);
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		helloLabel = new QLabel(centralWidget);
		helloLabel->setObjectName(QStringLiteral("helloLabel"));
		helloLabel->setGeometry(QRect(20, 80, 111, 16));
		helloLabel->setText("Hello ");
		lineEditWithName = new QLineEdit(centralWidget);
		lineEditWithName->setObjectName(QStringLiteral("lineEditWithName"));
		lineEditWithName->setGeometry(QRect(160, 80, 181, 20));

		std::string defaultLineEditName = boost::any_cast<std::string>(m_TransientDataCollection["NameForLineEdit"]);
		lineEditWithName->setText(QString(defaultLineEditName.c_str()));

		backButton = new QPushButton(centralWidget);
		backButton->setObjectName(QStringLiteral("backButton"));
		backButton->setGeometry(QRect(240, 180, 75, 23));
		backButton->setText("back");
		m_uMainWindow->setCentralWidget(centralWidget);
	}
	void BaseScene::mf_backButton()
	{
		const std::string c_szNextSceneName = "InitialScene";
		emit SceneChange(c_szNextSceneName);
	}
}
